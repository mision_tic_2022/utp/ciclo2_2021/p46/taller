/**
 * Autor:
 * Fecha:
 * Empresa:
 * Ciudad:
 * Descripción Proyecto: 
 */
public class Empleado {
    /**************
     * Atributos
     *************/
    private String nombre;
    private String apellido;
    private String cedula;
    private double sueldo;

    /****************
     * Constructor
     ****************/
    public Empleado(String nombre, String apellido, String cedula){
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
    }

    /***************
     * Consultores
     * Getters
     **************/
    public String getNombre(){
        return this.nombre;
    }
    public String getApellido(){
        return this.apellido;
    }
}
